
CREATE USER 'repl'@'%' IDENTIFIED BY 'MySQL1.0$';
--CREATE USER 'repl'@'%' IDENTIFIED WITH caching_sha2_password BY 'MySQL1.0$';
GRANT REPLICATION SLAVE ON *.* TO 'repl'@'%';

