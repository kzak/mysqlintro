# -*- mode: ruby -*-
# vi: set ft=ruby :
# https://www.vagrantup.com/docs/vagrantfile/tips.html#overwrite-host-locale-in-ssh-session
ENV["LC_ALL"] = "en_US.UTF-8"
# https://www.vagrantup.com/docs/vagrantfile/vagrant_version.html
Vagrant.require_version ">= 1.9.0"

# http://matthewcooper.net/2015/01/15/automatically-installing-vagrant-plugin-dependencies/
required_plugins = %w( vagrant-vbguest vagrant-reload )
required_plugins.each do |plugin|
	#exec "vagrant plugin install #{plugin}; vagrant #{ARGV.join(" ")}" unless Vagrant.has_plugin? plugin || ARGV[0] == 'plugin'
	#system "vagrant plugin install #{plugin}; vagrant #{ARGV.join(" ")}" unless Vagrant.has_plugin? plugin || ARGV[0] == 'plugin'
	#system "vagrant plugin install #{plugin}" unless Vagrant.has_plugin? plugin || ARGV[0] == 'plugin'
	unless Vagrant.has_plugin? plugin
		system("vagrant plugin install #{plugin}")
		#exec("vagrant", "#{ARGV.join(" ")}")
		#exec("vagrant #{ARGV.join(' ')}")
		# it never gets here if exec is used above
		abort("#{plugin} plugin was updated - please re-run:\n\n\tvagrant #{ARGV.join(" ")}")
	end
end

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  #config.vm.box = "centos/7"
  #config.vm.box_version = ">= 1611.01"
  config.vm.synced_folder ".", "/vagrant", type: "virtualbox"
  #config.vm.synced_folder ".", "/vagrant", nfs: true
  
  # example of ssh user/pass based auth
  #config.ssh.username = "root"
  #config.ssh.password = "redknee1"

  # port forwarding for the staging GUI
  #config.vm.network "forwarded_port", guest: 80, host: 8080

  # guests resources profiles definitions
  # https://github.com/stephenrlouie/PXE-Boot-VM/blob/master/Vagrantfile
  def client(config)
	config.vm.box = "TimGesekus/pxe-boot"
  	config.vm.provider "virtualbox" do |v|
		v.memory = 512
		v.cpus = 1
  		v.gui = true
		#v.customize ["modifyvm", :id, "--groups", "/MySQL_Introduction"]
	end
  end

  def server(config)
  	config.vm.box = "centos/7"
        config.vm.box_version = ">= 1703.01"
  	config.vm.synced_folder ".", "/vagrant", type: "virtualbox"
  	config.vm.provider "virtualbox" do |v|
		v.memory = 1024
		v.cpus = 1
		v.customize ["modifyvm", :id, "--groups", "/MySQL_Introduction"]
	end
  end

  def serverxl(config)
  	config.vm.provider "virtualbox" do |v|
		v.memory = 2048
		v.cpus = 2
		#v.customize ["modifyvm", :id, "--groups", "/MySQL_Introduction"]
	end
  end

  #config.vm.define "m1", primary: true 
  #config.vm.define "m2"
  #config.vm.define "m3"

  # https://www.vagrantup.com/docs/vagrantfile/tips.html#loop-over-vm-definitions
  (1..2).each do |i|
  	config.vm.define "m#{i}" do |node|

		server(node)
  		node.vm.hostname = "m#{i}"
 		#node.vm.network "private_network", ip: "192.168.101.10#{i}", virtualbox__intnet: "intnet-m"
 		node.vm.network "private_network", ip: "192.168.101.10#{i}"
  		#node.vm.network "forwarded_port", guest: 22, host: "220#{i}"
  		#node.vm.network "forwarded_port", guest: 80, host: 8080+i

  		shell_provisioners =  %w( download install_createrepo create_local_repo set_up_repos install_mysql_repo install_mysql update copy_mysql_config )
  		shell_provisioners.each do |shprov|
  			node.vm.provision shprov, type: "shell", path: "provision/shell/" + shprov + ".sh"
  		end

config.vm.provision "shell", inline: <<-SHELL
	#cp /vagrant/config/*.repo /etc/yum.repos.d/ 
	#dnf makecache
	#dnf -y update
	#dnf -y install mariadb-server-galera percona-xtrabackup
	#dnf -y install MariaDB-server MariaDB-backup
SHELL

  		node.vm.provider :virtualbox do |v|
  			v.name = "m#{i}"
  			v.gui = false
			v.customize ["modifyvm", :id, "--groups", "/MySQL_Introduction"]
  		end

  	end
  end

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  #config.vm.network "private_network", ip: "192.168.33.33/24"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  #config.vm.network "public_network", ip: "192.168.200.1/24", bridge: "Intel(R) 82579LM Gigabit Network Connection"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # https://github.com/dotless-de/vagrant-vbguest/blob/master/Readme.md
  config.vbguest.auto_update = true

  #config.trigger.after :up do
	#run "vagrant vbguest --auto-reboot --provision"
	#run "vagrant vbguest" :id # should consider single machine, not all of them
  #end

  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  #shell_provisioners =  %w( stage1 live hpe vmware stage2 )
  #shell_provisioners.each do |shprov|
  #	config.vm.provision shprov, type: "shell", path: "provision/" + shprov + ".sh"
  #end

  config.vm.post_up_message = "DONE"

end

