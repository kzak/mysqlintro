#cp /vagrant/config/mysqlrootpass /etc/
#chcon system_u:object_r:mysqld_etc_t:s0 /etc/mysqlrootpass
#chown root:root /etc/mysqlrootpass

cp -pfv /etc/my.cnf{,.original.`date +%Y%m%d-%H%M`} && \
	{
		cp -fv /vagrant/config/my.cnf.`uname -n` /etc/my.cnf
		chcon system_u:object_r:mysqld_etc_t:s0 /etc/my.cnf
		chown root:root /etc/my.cnf
	}
