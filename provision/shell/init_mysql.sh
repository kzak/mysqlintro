
newpass='MySQL1.0$'

if [[ `uname -n` != "m3" ]]
then

	systemctl enable mysqld
	systemctl start mysqld
	mysql --connect-expired-password -uroot -p`grep "temporary password is generated for root@localhost:" /var/log/mysqld.log | awk '{print $NF}' | tail -1` -e "ALTER USER 'root'@'localhost' IDENTIFIED BY '${newpass}';"

	mysql -uroot -p${newpass} < /vagrant/config/users.sql
fi

